<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::group(['prefix' => 'master', 'as' => 'master.'], function () {
    Route::resource('power_unit', 'Master\PowerUnitController');
    Route::get('power_unit_datatables', ['uses' => 'Master\PowerUnitController@datatables', 'as' => 'power_unit.datatables'] );
    Route::resource('maps_leaflet', 'Master\MapsLeafletController');
    Route::resource('query_data', 'Master\QueryDataController');
    Route::resource('number_pattern', 'Master\NumberPatternController');
    Route::get('pattern_one', ['uses' => 'Master\NumberPatternController@NumberPatternOne', 'as' => 'pattern_one'] );
    Route::get('pattern_two', ['uses' => 'Master\NumberPatternController@NumberPatternTwo', 'as' => 'pattern_two'] );
    Route::get('pattern_three', ['uses' => 'Master\NumberPatternController@NumberPatternThree', 'as' => 'pattern_three'] );
    Route::get('pattern_four', ['uses' => 'Master\NumberPatternController@NumberPatternFour', 'as' => 'pattern_four'] );
});

