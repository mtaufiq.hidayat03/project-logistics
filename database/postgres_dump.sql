--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4
-- Dumped by pg_dump version 12.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: corporation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.corporation (
    id_corporation integer NOT NULL,
    corporation character varying(300) NOT NULL,
    insert_user integer NOT NULL,
    insert_date date NOT NULL,
    update_user integer,
    update_date date
);


ALTER TABLE public.corporation OWNER TO postgres;

--
-- Name: corporation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.corporation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.corporation_id_seq OWNER TO postgres;

--
-- Name: corporation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.corporation_id_seq OWNED BY public.corporation.id_corporation;


--
-- Name: gn_md_lookup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gn_md_lookup (
    lookup_lines_id integer NOT NULL,
    lookup_id integer NOT NULL,
    lookup_lines_code character varying(100) NOT NULL,
    description character varying(300) NOT NULL,
    effective_form date NOT NULL,
    effective_to date NOT NULL,
    insert_user integer NOT NULL,
    insert_time date NOT NULL
);


ALTER TABLE public.gn_md_lookup OWNER TO postgres;

--
-- Name: gn_md_lookup_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gn_md_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gn_md_lookup_id_seq OWNER TO postgres;

--
-- Name: gn_md_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gn_md_lookup_id_seq OWNED BY public.gn_md_lookup.lookup_lines_id;


--
-- Name: gn_mh_lookup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gn_mh_lookup (
    lookup_id integer NOT NULL,
    lookup_code character varying(100) NOT NULL,
    description character varying(300) NOT NULL,
    insert_user integer NOT NULL,
    insert_date date NOT NULL
);


ALTER TABLE public.gn_mh_lookup OWNER TO postgres;

--
-- Name: gn_mh_lookup_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gn_mh_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gn_mh_lookup_id_seq OWNER TO postgres;

--
-- Name: gn_mh_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gn_mh_lookup_id_seq OWNED BY public.gn_mh_lookup.lookup_id;


--
-- Name: location; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.location (
    id_location integer NOT NULL,
    location_name character varying(400) NOT NULL,
    city character varying(200) NOT NULL,
    province character varying(100) NOT NULL,
    latitude double precision NOT NULL,
    longitude double precision NOT NULL,
    insert_user integer NOT NULL,
    insert_date date NOT NULL,
    update_user integer,
    update_date date
);


ALTER TABLE public.location OWNER TO postgres;

--
-- Name: location_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.location_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.location_id_seq OWNER TO postgres;

--
-- Name: location_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.location_id_seq OWNED BY public.location.id_location;


--
-- Name: power_unit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.power_unit (
    id_power_unit integer NOT NULL,
    power_unit_num character varying(20) NOT NULL,
    description character varying(400),
    id_corporation integer NOT NULL,
    id_location integer NOT NULL,
    id_power_unit_type integer NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    insert_user integer NOT NULL,
    insert_date date NOT NULL,
    update_user integer,
    update_date date
);


ALTER TABLE public.power_unit OWNER TO postgres;

--
-- Name: power_unit_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.power_unit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.power_unit_id_seq OWNER TO postgres;

--
-- Name: power_unit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.power_unit_id_seq OWNED BY public.power_unit.id_power_unit;


--
-- Name: power_unit_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.power_unit_type (
    id_power_unit_type integer NOT NULL,
    power_unit_type_xid character varying(100) NOT NULL,
    description character varying(200) NOT NULL,
    insert_user integer NOT NULL,
    insert_date date NOT NULL,
    update_user integer,
    update_date date
);


ALTER TABLE public.power_unit_type OWNER TO postgres;

--
-- Name: power_unit_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.power_unit_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.power_unit_type_id_seq OWNER TO postgres;

--
-- Name: power_unit_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.power_unit_type_id_seq OWNED BY public.power_unit_type.id_power_unit_type;


--
-- Name: corporation id_corporation; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.corporation ALTER COLUMN id_corporation SET DEFAULT nextval('public.corporation_id_seq'::regclass);


--
-- Name: gn_md_lookup lookup_lines_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gn_md_lookup ALTER COLUMN lookup_lines_id SET DEFAULT nextval('public.gn_md_lookup_id_seq'::regclass);


--
-- Name: gn_mh_lookup lookup_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gn_mh_lookup ALTER COLUMN lookup_id SET DEFAULT nextval('public.gn_mh_lookup_id_seq'::regclass);


--
-- Name: location id_location; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.location ALTER COLUMN id_location SET DEFAULT nextval('public.location_id_seq'::regclass);


--
-- Name: power_unit id_power_unit; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.power_unit ALTER COLUMN id_power_unit SET DEFAULT nextval('public.power_unit_id_seq'::regclass);


--
-- Name: power_unit_type id_power_unit_type; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.power_unit_type ALTER COLUMN id_power_unit_type SET DEFAULT nextval('public.power_unit_type_id_seq'::regclass);


--
-- Data for Name: corporation; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.corporation VALUES (1, 'PT PUNINAR JAYA', 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.corporation VALUES (2, 'PT. PUNINAR INFINITE RAYA', 1, '2018-12-20', 2, '2018-12-21');


--
-- Data for Name: gn_md_lookup; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.gn_md_lookup VALUES (1, 1, 'M', 'Male', '2018-12-20', '2020-12-20', 1, '2018-12-20');
INSERT INTO public.gn_md_lookup VALUES (2, 1, 'F', 'Female', '2018-12-20', '2020-12-20', 1, '2018-12-20');
INSERT INTO public.gn_md_lookup VALUES (3, 2, 'ISLAM', 'ISLAM', '2018-12-01', '2020-12-20', 1, '2018-12-21');
INSERT INTO public.gn_md_lookup VALUES (4, 2, 'BUDHA', 'BUDHA', '2018-12-01', '2020-12-20', 1, '2018-12-20');
INSERT INTO public.gn_md_lookup VALUES (5, 2, 'HINDU', 'HINDU', '2018-12-01', '2020-12-20', 1, '2018-12-20');
INSERT INTO public.gn_md_lookup VALUES (6, 3, 'PRM_CLS_AR', 'Parameter Closing AR', '2018-12-01', '2018-12-31', 1, '2018-12-21');


--
-- Data for Name: gn_mh_lookup; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.gn_mh_lookup VALUES (1, 'MST_GENDER', 'Master Gender', 1, '2018-12-20');
INSERT INTO public.gn_mh_lookup VALUES (2, 'MST_RELIGION', 'Master Religion', 1, '2018-12-20');
INSERT INTO public.gn_mh_lookup VALUES (3, 'CLS_AR', 'Closing AR', 1, '2018-12-21');


--
-- Data for Name: location; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.location VALUES (1, 'POOL CILEGON', 'CILEGON', 'BANTEN', -6.002534, 106.011124, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.location VALUES (2, 'SURABAYA POOL', 'PANDAAN', 'JAWA TIMUR', -7.6503, 112.7057, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.location VALUES (3, 'POOL CAKUNG', 'CAKUNG', 'DKI JAKARTA', -6.172035, 106.942108, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.location VALUES (4, 'POOL NAGRAK', 'NAGRAK', 'DKI JAKARTA', -6.116907, 106.942471, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.location VALUES (5, 'POOL MEDAN', 'MEDAN', 'SUMATRA UTARA', 3.59, 98.67802, 1, '2018-12-20', 2, '2018-12-21');


--
-- Data for Name: power_unit; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.power_unit VALUES (2, 'B 9913 SYM', 'HINO WINGBOX/FM 260 JD', 1, 2, 2, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (3, 'B 9916 SYM', 'HINO WINGBOX/FM 260 JD', 1, 3, 3, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (4, 'B 9918 SYM', 'HINO WINGBOX/FM 260 JD', 1, 4, 4, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (5, 'B 9008 TEH', 'HINO _/FM 320 P', 1, 5, 5, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (6, 'B 9985 ZJ', 'HINO _/FM 320 P', 1, 1, 6, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (7, 'B 9059 TIN', 'HINO MIXER/FM 260 JM', 1, 2, 7, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (8, 'B 9053 TIN', 'HINO MIXER/FM 260 JM', 1, 3, 1, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (9, 'B 9100 TIN', 'HINO MIXER/FM 260 JM', 1, 4, 2, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (10, 'B 9133 TIN', 'HINO MIXER/FM 260 JM', 1, 5, 3, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (11, 'B 9136 TIN', 'HINO MIXER/FM 260 JM', 2, 1, 4, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (12, 'B 9146 TIN', 'HINO MIXER/FM 260 JM', 2, 2, 5, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (13, 'B 9281 TEH', 'HINO _/FM 320 P', 2, 3, 6, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (14, 'B 9282 TEH', 'HINO _/FM 320 P', 2, 4, 7, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (15, 'B 9316 TEH', 'HINO _/FM 320 P', 2, 5, 1, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (16, 'B 9351 TEH', 'HINO _/FM 320 P', 2, 1, 2, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (17, 'B 9357 TEH', 'HINO _/FM 320 P', 2, 2, 3, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (18, 'B 9313 TYU', 'HINO MIXER/FM 260 JD', 2, 3, 4, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (19, 'B 9180 TIN', 'HINO MIXER/FM 260 JM', 2, 4, 5, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (20, 'B 9177 TIN', 'HINO MIXER/FM 260 JM', 2, 5, 6, true, 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit VALUES (1, 'B 9914 SYM', 'HINO WINGBOX/FM 260 JD', 1, 1, 1, true, 1, '2018-12-20', 2, '2018-12-21');


--
-- Data for Name: power_unit_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.power_unit_type VALUES (1, 'ENGKEL', 'PRIME_MOVER ENGKEL NON KAROSERI', 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit_type VALUES (2, 'TRONTON', 'PRIME_MOVER TRONTON NON KAROSERI', 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit_type VALUES (3, 'TOWING_CDD', 'RIGID CDD JENIS KAROSERI TOWING', 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit_type VALUES (4, 'CAR-CARRIER_ENGKEL', 'PRIME MOVER ENGKEL JENIS KAROSERI CAR CARRIER', 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit_type VALUES (5, 'MOTOR-CARRIER_ENGKEL', 'PRIME MOVER ENGKEL JENIS KAROSERI MOTOR CARRIER', 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit_type VALUES (6, 'FLAT-DECK_TRONTON', 'RIGID TRONTON JENIS KAROSERI FLAT DECK', 1, '2018-12-20', 2, '2018-12-21');
INSERT INTO public.power_unit_type VALUES (7, 'MIXER_TRONTON', 'RIGID TRONTON JENIS KAROSERI MIXER', 1, '2018-12-20', 2, '2018-12-21');


--
-- Name: corporation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.corporation_id_seq', 3, true);


--
-- Name: gn_md_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gn_md_lookup_id_seq', 1, false);


--
-- Name: gn_mh_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gn_mh_lookup_id_seq', 1, false);


--
-- Name: location_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.location_id_seq', 6, false);


--
-- Name: power_unit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.power_unit_id_seq', 1, false);


--
-- Name: power_unit_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.power_unit_type_id_seq', 1, false);


--
-- Name: corporation corporation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.corporation
    ADD CONSTRAINT corporation_pkey PRIMARY KEY (id_corporation);


--
-- Name: gn_md_lookup gn_md_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gn_md_lookup
    ADD CONSTRAINT gn_md_lookup_pkey PRIMARY KEY (lookup_lines_id);


--
-- Name: gn_mh_lookup gn_mh_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gn_mh_lookup
    ADD CONSTRAINT gn_mh_lookup_pkey PRIMARY KEY (lookup_id);


--
-- Name: location location_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_pkey PRIMARY KEY (id_location);


--
-- Name: power_unit power_unit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.power_unit
    ADD CONSTRAINT power_unit_pkey PRIMARY KEY (id_power_unit);


--
-- Name: power_unit_type power_unit_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.power_unit_type
    ADD CONSTRAINT power_unit_type_pkey PRIMARY KEY (id_power_unit_type);


--
-- PostgreSQL database dump complete
--

