<?php
function pattern_one($num) {
    $string = " ";
    for ($a = $num; $a >= 1; $a--) {
        for ($b = 1; $b <= $a; $b++) {
            echo $b.$string;
        }
        echo "</br>";
    }
}

function pattern_two($num) {
    $string = " ";
    for ($a = $num; $a >= 1; $a--) {
        for ($b = $a; $b >= 1; $b--) {
            echo $b.$string;
        }
        echo "</br>";
    }
}

function pattern_three($num) {
    $n = $num;
    $string = "";
    for ($i = 1; $i <= $n; $i++) {
      for ($j = 1; $j < $n - $i + 1; $j++) {
        $string .= "&nbsp;";
      }
      for ($k = 1; $k <= 2 * $i - 1; $k++) {
        $pyramid = (str_pad('', $num - $i));
        for($ii = $num; $ii > 0; $ii--) {
            $pyramid .= $ii-1 . "&nbsp;";
        }
        //for($ii = $i-1; $ii > 0; $ii--) {
        //    $pyramid .= $ii . "&nbsp;";
        //}
      }
      $string .= $pyramid . PHP_EOL. "<br/>";
    }
    echo "<pre>".$string."</pre>";
}

function pattern_four($num) {
    $n = $num;
    $string = "";
    $string2 = "";
    for ($i = 1; $i <= $n; $i++) {
      for ($j = 1; $j < $n - $i + 1; $j++) {
        $string .= "&nbsp;";
      }
      for ($k = 1; $k <= 2 * $i - 1; $k++) {
        $pyramid = (str_pad('', $num - $i));
        for($ii = 1; $ii <= $i; $ii++) {
            $pyramid .= $ii . "&nbsp;";
        }
        for($ii = $i-1; $ii > 0; $ii--) {
            $pyramid .= $ii . "&nbsp;";
        }
      }
      $string .= $pyramid . "<br/>";
    }
    for($a=$num-1; $a>=1; $a--) {
        for ($b = 1; $b < $a - $b + 1; $b++) {
            $string2 .= "&nbsp;";
        }
            $reversed = (str_pad('', $num - $i));
            for ($b = 1; $b <= $a; $b++) {
                $reversed .= $b . "&nbsp;";
            }
            for ($b = $a - 1; $b >= 1; $b--) {
                $reversed .= $b . "&nbsp;";
            }
        $string2 .= $reversed."<br/>";
    }

    echo "<pre>".$string.$string2."</pre>";
}
