<?php
namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NumberPatternController extends Controller {

    public function NumberPatternOne() {
        return view('number_pattern.one');
    }

    public function NumberPatternTwo() {
        return view('number_pattern.two');
    }

    public function NumberPatternThree() {
        return view('number_pattern.three');
    }

    public function NumberPatternFour() {
        return view('number_pattern.four');
    }

}
