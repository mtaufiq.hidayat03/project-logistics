<?php
namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Location;
use Illuminate\Http\Request;

class MapsLeafletController extends Controller {

    public function index(Request $request) {

        return view('maps-leaflet.index');

    }

    public function listCity(Request $request) {
        try {
            $city = Location::orderBy('id_location', 'ASC')->get()->all();
            return response()->json(["status" => 200, "result" => $city, "message" => "OK"]);
        } catch (\Exception $e) {
            return response()->json(["status" => 400, "result" => NULL, "message" => "Error = " . $e]);
        }
    }

}
