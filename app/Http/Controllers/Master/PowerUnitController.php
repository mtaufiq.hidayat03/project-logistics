<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Corporation;
use App\Models\Location;
use App\Models\PowerUnitType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Vinkla\Hashids\Facades\Hashids;
use Yajra\DataTables\Facades\DataTables;

class PowerUnitController extends Controller
{

    public function index(Request $request)
    {

        return view('power_unit.index');

    }

    public function datatables(Request $request)
    {

        $result = DB::table('power_unit')
            ->select('power_unit.power_unit_num AS power_unit_num', 'power_unit.description AS description', 'corporation.corporation as corporation', 'location.location_name as location', 'power_unit_type.power_unit_type_xid as power_unit_type_xid', 'power_unit.is_active AS is_active', 'power_unit.insert_date AS insert_date', 'power_unit.update_date AS update_date')
            ->leftJoin('corporation', 'corporation.id_corporation', '=', 'power_unit.id_corporation')
            ->leftJoin('location', 'location.id_location', '=', 'power_unit.id_location')
            ->leftJoin('power_unit_type', 'power_unit_type.id_power_unit_type', '=', 'power_unit.id_power_unit_type')
            ->where('power_unit.is_active', '=', TRUE);

        return DataTables::of($result)
            ->addColumn('action', function ($result) {
                /*
                $url_edit = "<a href='".route('ic.setting_library_online.edit', ['id' => Hashids::encode($result->id)])."' title='".trans('app.edit_title')."' data-toggle='tooltip' class='btn btn-outline'><span class='ti-settings icon-lg'></span> </a>";
                $url_delete = "<form class='delete' action='" . route('ic.setting_library_online.delete', ['id' => Hashids::encode($result->id)]) . "' method='POST'>
                    " . csrf_field() . "<button type='submit' class='btn btn-outline text-danger' title='" . trans('app.delete_title') . "' data-toggle='tooltip'><i class='ti-trash icon-xl'></i></button></form>";
                    */
                $url_delete = "";
                $url_edit = "";
                return
                    '<div class="btn-group">'
                    . $url_edit . $url_delete .
                    '</div>';
            })
            ->editColumn('power_unit_num', function ($result) {
                return $result->power_unit_num ? $result->power_unit_num : '-';
            })
            ->filterColumn('power_unit_num', function ($query, $keyword) {
                $sql = "power_unit.power_unit_num ilike ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->editColumn('description', function ($result) {
                return $result->description ? $result->description : '-';
            })
            ->filterColumn('description', function ($query, $keyword) {
                $sql = "power_unit.description ilike ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->editColumn('corporation', function ($result) {
                return $result->corporation ? $result->corporation : '-';
            })
            ->filterColumn('corporation', function ($query, $keyword) {
                $sql = "corporation.corporation ilike ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->editColumn('location', function ($result) {
                return $result->location ? $result->location : '-';
            })
            ->filterColumn('location', function ($query, $keyword) {
                $sql = "location.location_name ilike ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->editColumn('power_unit_type_xid', function ($result) {
                return $result->power_unit_type_xid ? $result->power_unit_type_xid : '-';
            })
            ->filterColumn('power_unit_type_xid', function ($query, $keyword) {
                $sql = "power_unit_type.power_unit_type_xid ilike ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->editColumn('is_active', function ($result) {
                return $result->is_active == "TRUE" ? "AKTIF" : 'TIDAK AKTIF';
            })
            ->editColumn('insert_date', function ($result) {
                setlocale(LC_TIME, 'id_ID.utf8');
                return $result->insert_date ? with(new Carbon($result->insert_date))->formatLocalized('%A, %d %B %Y') : '-';
            })
            ->editColumn('update_date', function ($result) {
                setlocale(LC_TIME, 'id_ID.utf8');
                return $result->update_date ? with(new Carbon($result->update_date))->formatLocalized('%A, %d %B %Y') : '-';
            })
            ->make(true);
    }

    public function create(Request $request)
    {
        $corporation = Corporation::select('corporation','id_corporation')->get();
        $location = Location::select(DB::raw("CONCAT(location_name,' - ',city,' - ',province) AS loc_name"),"id_location")->get();
        $powerUnitType = PowerUnitType::select('power_unit_type_xid','id_power_unit_type')->get();
        return view('power_unit.create',compact('corporation', 'location','powerUnitType'));

    }

    public function store(Request $request)
    {
        $data['nik'] = $request->get('nik_employee');
        $data['name_employee'] = $request->get('name_employee');
        $data['status'] = $request->get('status_employee');
        $data['created_by'] = "1";
        $data['created_at'] = Carbon::now();
        Corporation::create($data);
        return redirect()->route('master.employee.index')->with(['success' => 'Data pegawai sukses disimpan!']);
    }

    public function edit($id)
    {

        return view('power_unit.edit');

    }

    public function update(Request $request)
    {

    }

    public function delete(Request $request)
    {

    }

}
