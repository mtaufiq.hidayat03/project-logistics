<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PowerUnit extends Model {

    protected $primaryKey = 'id_power_unit';

    protected $table = 'power_unit';

    public $timestamps = false;

    protected $dateFormat = 'Y-m-d H:i:sO';

    protected $fillable = ['power_unit_num', 'description', 'id_corporation', 'id_location', 'id_poer_unit_type', 'is_active', 'insert_user', 'insert_date', 'update_user', 'update_date'];
}
