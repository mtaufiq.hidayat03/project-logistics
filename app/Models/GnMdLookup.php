<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GnMdLookup extends Model {

    protected $primaryKey = 'lookup_lines_id';

    protected $table = 'gn_md_lookup';

    protected $dateFormat = 'Y-m-d H:i:sO';

    public $timestamps = false;

    protected $fillable = ['lookup_id','lookup_lines_code', 'description', 'effective_from', 'effective_to','insert_date', 'insert_time'];
}
