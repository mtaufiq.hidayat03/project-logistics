<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {

    protected $primaryKey = 'id_location';

    protected $table = 'location';

    protected $dateFormat = 'Y-m-d H:i:sO';

    public $timestamps = false;

    protected $fillable = ['location_name','city', 'province', 'latitude', 'longitude', 'insert_user','insert_date', 'update_user', 'update_date'];
}
