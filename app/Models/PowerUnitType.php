<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PowerUnitType extends Model {

    protected $primaryKey = 'id_power_unit_type';

    protected $table = 'power_unit_type';

    public $timestamps = false;

    protected $dateFormat = 'Y-m-d H:i:sO';

    protected $fillable = ['power_unit_type_xid', 'description', 'insert_user','insert_date', 'update_user', 'update_date'];
}
