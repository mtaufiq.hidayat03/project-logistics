<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Corporation extends Model {

    protected $primaryKey = 'id_corporation';

    protected $table = 'corporation';

    protected $dateFormat = 'Y-m-d H:i:sO';

    public $timestamps = false;

    protected $fillable = ['corporation', 'insert_user','insert_date', 'update_user', 'update_date'];
}
