<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GnMhLookup extends Model {

    protected $primaryKey = 'lookup_id';

    protected $table = 'gn_mh_lookup';

    protected $dateFormat = 'Y-m-d H:i:sO';

    public $timestamps = false;

    protected $fillable = ['lookup_code', 'description', 'insert_user', 'insert_date'];
}
