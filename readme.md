# Project Logistics

All you need before run :

```bash
1. Install git: https://git-scm.com/

2. Install postgresql version 12: https://www.postgresql.org/download/

3. Install apache web server: https://httpd.apache.org/download.cgi

4. Install PHP version 7.4: https://www.php.net/downloads.php

5. Install Node.js: https://nodejs.org/en/download/

6. Install composer: https://getcomposer.org/download/

7. Install all php depencies for laravel : OpenSSL PHP Extension, PDO PHP Extension, Mbstring PHP Extension, Tokenizer PHP Extension, XML PHP Extension

8. Git clone from this url: https://gitlab.com/mtaufiq.hidayat03/project-logistics.git

9. Database using postgresql and create database with name : logistics

10. Export sql in folder database.

11. Create .env file: copy all entries of env.example and paste into .env file

```

All you need is to run these commands:
```bash
1. Open folder project in terminal or command prompt.

2. Run command to install composer in project : composer install 

3. Run command to install node package manager (npm) in project : npm install

3. Generate key : php artisan key:generate

4. *) Run : php artisan serve 

   *) Run with custom port : php artisan serve --port=8031

```

Demo App : [Project Logistics](http://localhost)

Demo App With Custom Port: [Project Logistics](http://localhost:8031)
