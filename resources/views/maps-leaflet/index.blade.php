@extends('layout.default')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
        </div>
    </section>
    <section class="content">
        <div class="card card-blue">
            <div class="card-header">
                <h3 class="card-title"><i class="fas fa-th"></i> Maps Using Leaflet</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div id="mapid" class="center-block" style="width: 100%; height: 600px;"></div>
            </div>
        </div>
    </section>
@endsection
@section('css')
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            var mymap = L.map('mapid');
            var LeafIcon = L.Icon.extend({options: {popupAnchor: [-5, -10]}});
            var myicon = new LeafIcon({
                iconUrl: "{{ asset('images/favIcon.png') }}",
                iconSize: [35, 35],
                iconAnchor: [15, 20],
            });
            L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
                zoomControl: false,
                minZoom: 0,
                maxZoom: 18,
                animate: true,
                scrollWheelZoom: 1,
            }).addTo(mymap);
            mymap.setView(new L.LatLng(-1.548926, 105.0148634), 6);
            $.ajax({
                processData: false,
                contentType: false,
                headers: {'X-CSRF-TOKEN': $('#token').val()},
                url: '{{ route('master.api.list_city') }}',
                dataType: 'json',
                type: 'GET',
                beforeSend: function () {
                    Swal.fire({
                        title: "Loading...",
                        text: "",
                        didOpen: () => {
                            Swal.showLoading();
                        },
                    });
                },
                success: function (data) {
                    if (data['status'] !== 200) {
                        Swal.fire({
                            type: (data['status'] !== 200) ? 'error' : 'success',
                            html: '<h5>' + data['message'] + '</h5>',
                        });
                    } else {
                        Swal.close();
                    }
                    for (let i = 0; i < data['result'].length; i++) {
                        let calData = data['result'][i];
                        L.marker([calData.latitude, calData.longitude], {icon: myicon}).addTo(mymap)
                            .bindPopup(calData.location_name + '<br>' + calData.city + ' - ' + calData.province);
                    }
                },
                error: function (data) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                    });
                }
            });

        });
    </script>
@stop

