<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', '') }}</title>

    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link rel="icon" href="{!! asset('images/favIcon.png') !!}"/>
    @yield('css')

</head>
<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
            @include('layout.partials.navbar')
            @include('layout.partials.sidebar_menu')
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @yield('content')
            </div>
            <!-- /.content-wrapper -->
        @include('layout.partials.footer')
    </div>
</body>
<style>
    .card-title { font-size: 1.25rem !important;}
    .invalid-feedback {font-size: 100% !important;}
</style>
<script src="{{ asset('js/popper.js') }}"></script>
<script src="{{ asset('/js/app.js') }}"></script>

<script>
    $(document).ready(function() {
        $('.dropdown.comments').click(function(){
            $('.dropdown-menu.comments').toggleClass('show');
        });
        $('.dropdown.notifications').click(function(){
            $('.dropdown-menu.notifications').toggleClass('show');
        });
        $('ul').Treeview();
    });
</script>
@yield('js')
</html>
