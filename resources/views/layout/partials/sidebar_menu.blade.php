<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-blue elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link" style="background-color: #007bff !important;">
        <span class="brand-text font-weight-light" style="font-size: 1rem !important; font-weight: 600!important;">Project of Logistics </span>&nbsp;
        <img src="{!! asset('images/favIcon.png') !!}" width="50" height="50"/>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="../../dist/img/avatar5.png" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Taufik Hidayat</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="/" class="nav-link <?php echo request()->path() == "/" ? 'active' : '' ?>">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Beranda
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('master.power_unit.index') }}"
                       class="nav-link <?php echo request()->path() == "master/power_unit" || request()->path() == "master/power_unit/create" || request()->path() == "master/power_unit/edit" ? 'active' : '' ?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            CRUD Power Unit (Q 1)
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('master.maps_leaflet.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Maps Using Leaflet(Q 2)
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">
                        <i class="nav-icon fas fa-tree"></i>
                        <p>
                            CRUD Power Unit 2(Q 3)
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">
                        <i class="nav-icon fas fa-hand-point-up"></i>
                        <p>
                            API (Q 4)
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('master.query_data.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Query Data (Q 5)
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>CRUD Lookup (Q 6)</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-code"></i>
                        <p>
                            Number Pattern (Q 7)
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" style="display: none;">
                        <li class="nav-item">
                            <a href="{{ route('master.pattern_one') }}" class="nav-link ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Question 1</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('master.pattern_two') }}" class="nav-link ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Question 2</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('master.pattern_three') }}" class="nav-link ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Question 3</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('master.pattern_four') }}" class="nav-link ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Question 4</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
