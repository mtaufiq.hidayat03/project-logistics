@extends('layout.default')
@section('content')
    <section class="content-header">
    </section>

    <section class="content">
        <div class="card card-blue">
            <div class="card-header">
                <h3 class="card-title">Home</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <h3>Welcome to </h3><br/><br/><h5>Project of Logistics</h5>
                <img src="{!! asset('images/logo_puninar.png') !!}" />
            </div>
        </div>
    </section>
@stop
@section('css')
@stop
@section('js')
@stop
