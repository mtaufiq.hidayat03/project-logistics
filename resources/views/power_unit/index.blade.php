@extends('layout.default')

@section('content')
    <section class="content-header">
    </section>
    <section class="content">
        <div class="card card-blue">
            <div class="card-header">
                <h3 class="card-title"><i class="fas fa-tachometer-alt"></i> Data Power Unit</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div>
                    <a href="{{ route('master.power_unit.create') }}" class="btn btn-success"><i class="fas fa-plus"></i> Tambah Data</a>
                </div>
                <br/>
                    <table id="dataTables" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Power Unit</th>
                                <th>Description</th>
                                <th>Corporation</th>
                                <th>Location</th>
                                <th>Power Unit Type</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
            </div>
        </div>
    </section>
@endsection

@section('css')
<link href="{{ asset('/css/custom.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script>
    $(document).ready(function() {
        var table = $('#dataTables').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            pageLength: 10,
            lengthMenu: [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
            ajax: '{{ route('master.power_unit.datatables') }}',
            columns: [
                {data: 'power_unit_num', name: 'power_unit_num'},
                {data: 'description', name: 'description'},
                {data: 'corporation', name: 'corporation'},
                {data: 'location', name: 'location'},
                {data: 'power_unit_type_xid', name: 'power_unit_type_xid'},
                {data: 'is_active', name: 'is_active'},
                {data: 'insert_date', name: 'insert_date'},
                {data: 'update_date', name: 'update_date'},
                {data: 'action', name: 'action', orderable: false, searchable: false},

            ],
            order: [[ 2, 'asc' ], [ 3, 'asc' ]],
            columnDefs: [
                { "width": "10%", "targets": 0 },
                { "width": "15%", "targets": 1 },
                { "width": "10%", "targets": 2 },
                { "width": "15%", "targets": 3 },
                { "width": "10%", "targets": 4 },
                { "width": "8%", "targets": 5 },
                { "width": "13%", "targets": 6 },
                { "width": "13%", "targets": 7 },
                { "width": "13%", "targets": 8 },
            ],
            fnInitComplete: function() {
                $('.dataTables_scrollHead').css('overflow', 'auto');
                $('.dataTables_scrollHead').on('scroll', function () {
                    $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
                });
                $('.dataTables_scrollBody').on('scroll', function () {
                    $('.dataTables_scrollHead').scrollLeft($(this).scrollLeft());
                });
            },
            oLanguage: {
                sProcessing: '<span style="font-size: 16px; font-weight: 800; margin-bottom: 30px;">Please wait...</span><span><img src="{!! asset('images/loading.gif') !!}" width="50px" height="50px"/></span>',
            },
        });
    });
</script>
@stop

