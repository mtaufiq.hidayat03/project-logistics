@extends('layout.default')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('master.power_unit.index') }}">Power Unit</a></li>
                        <li class="breadcrumb-item active">Buat Data Power Unit</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-blue">
            <div class="card-header">
                <h3 class="card-title"><i class="fas fa-tachometer-alt"></i> Buat Data Power Unit</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div>
                    <a href="{{ route('master.power_unit.index') }}" class="btn btn-warning"><i class="fas fa-arrow-left"></i> Kembali</a>
                </div>
                <br/>
                {!! Form::open(['method' => 'POST', 'route' => ['master.power_unit.store'], 'files' => true, 'id' => 'master_power_unit_data']) !!}

                <div class="form-group row">
                    <label for="nik" class="col-sm-3 col-form-label text-right">Power Unit Num</label>
                    <div class="col-sm-8">
                        <input type="text" required="" class="form-control" id="power_unit_num" name="power_unit_num" placeholder="Masukkan power unit num">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-sm-3 col-form-label text-right">Description</label>
                    <div class="col-sm-8">
                        <input type="text" required="" class="form-control" id="description" name="description" placeholder="Masukkan description">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-sm-3 col-form-label text-right">Corporation</label>
                    <div class="col-sm-8">
                        <select class="form-control select2" name="id_corporation" id="id_corporation">
                            <option value="0">Choose one corporation</option>
                            @foreach($corporation as $corp)
                                <option value="{{ $corp->id_corporation }}">{{ $corp->corporation }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-sm-3 col-form-label text-right">Location</label>
                    <div class="col-sm-8">
                        <select class="form-control select2" name="id_location" id="id_location">
                            <option value="0">Choose one location</option>
                            @foreach($location as $loc)
                                <option value="{{ $loc->id_location }}">{{ $loc->loc_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-sm-3 col-form-label text-right">Power Unit Type</label>
                    <div class="col-sm-8">
                        <select class="form-control select2" name="id_power_unit_type" id="id_power_unit_type">
                            <option value="0">Choose one Power Unit Type</option>
                            @foreach($powerUnitType as $put)
                                <option value="{{ $put->id_power_unit_type }}">{{ $put->power_unit_type_xid }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="status" class="col-sm-3 col-form-label text-right">Status Aktif</label>
                    <div class="col-sm-8">
                        <select class="custom-select rounded-0" id="is_active" name="is_active">
                            <option value="" disabled selected hidden>PILIH SALAH SATU</option>
                            <option value="true">AKTIF</option>
                            <option value="false">TIDAK AKTIF</option>
                        </select>
                    </div>
                </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer" style="text-align: center">
                <button type="submit" value="save" name="save" id="btn-submit" class="btn btn-danger"><i class="fas fa-save mR-10"></i> Simpan</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="{{ route('master.power_unit.index') }}" class="btn btn-outline-dark"><i class="fas fa-window-close"></i> Batalkan</a>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@stop
@section('css')
<style>
    .form-group {margin-bottom: 3rem !important;}
</style>
@stop
@section('js')
<script>
    $(document).ready(function() {
            $('#id_corporation').select2({
                placeholder: "Please select corporation..."
            });
            $('#master_power_unit_data').validate({
                onkeyup: false,
                ignore: [],
                errorClass: "error",
                rules: {
                    power_unit_num: {required : true},
                    description: {required : true},
                    id_corporation: {required : true},
                },
                messages: {
                    power_unit_num: "Tolong diisi",
                    description: "Tolong diisi",
                    id_corporation: "Tolong dipilih",
                },
                 errorPlacement: function (error, element) {
                  error.addClass('invalid-feedback');
                  error.insertAfter(element);
                },
                highlight: function (element, errorClass, validClass) {
                  $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                  $(element).removeClass('is-invalid');
                },
                submitHandler: function (form) {
                    //form.submit();
                    $.ajax({
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            var percentComplete;
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    $('.progress-bar').attr('aria-valuenow', percentComplete);
                                    $('.progress-bar').attr('style', 'width:' + percentComplete + '%');
                                    $('div.progress-bar').text(percentComplete + '%');
                                }
                            }, false);

                            return xhr;
                        },
                        url: '{{ route('master.power_unit.store') }}',
                        type: "POST",
                        cache: false,
                        processData: false,
                        contentType: false,
                        data: new FormData(form),
                        headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                        beforeSend: function () {
                            Swal.fire({
                                width: 700,
                                title: 'Document is saving',
                                html: '<br><div class="progress" style="height: 40px;"><div class="progress-bar progress-bar-striped progress-bar-animated font-progressbar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div></div><br>',
                                showConfirmButton: false,
                                allowOutsideClick: false,
                            });
                            window.onbeforeunload = function () {
                                return "Are you sure you want to leave? The data maybe will not saved";
                            }
                        },
                        success: function (data) {
                            Swal.fire({
                                width: 700,
                                type: (data['status'] === 200) ? 'success' : 'error',
                                html: '<h5>' + data['message'] + '</h5>',
                            }).then(function () {
                                window.location.href = (data['status'] === 200) ? data['payload'] : '{{ route('master.power_unit.index') }}';
                            });
                        },
                        error: function (jqXHR, textStatus) {
                            $("#lo_file").val(null);
                            const result = JSON.parse(jqXHR.responseText);
                            Swal.fire({
                                width: 700,
                                type: 'error',
                                html: '<h5>' + (result['message'] ? result['message'] : 'Error, please contact Administrator') + '</h5>',
                            }).then(function () {
                                window.location.href = '{{ route('master.power_unit.index') }}';
                            });
                            return false;
                            window.onbeforeunload = null;
                        },
                        complete: function () {
                            window.onbeforeunload = null;
                        }
                    });
                    return false;
                }
            });
            $("select").on("select2:close", function (e) {
                $(this).valid();
            });
    });
</script>
@stop
